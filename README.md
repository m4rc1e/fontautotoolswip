# Font Auto Tools

Auto generate metrics, mark positioning, glyph identification.


## Installation

```
$ virtualenv venv
$ source venv/bin/activate
$ pip install fontautotools
```


## Test a font's glyphs are correct

```
$ isglyph font.ttf
```


## Generating training data

```
$ fat-train -f [fonts.ttf]
# recursively add fonts from a folder
$ fat-train -r /dir/to/fonts/
```


## How much training data has been used

- 2017/08/30: Every font and encoded glyph from [fonts.google.com](https://fonts.google.com)


## I'd like to contribute my fonts

Great! this will help us improve the accuracy of the model.