import sys
from setuptools import setup, find_packages, Command
from distutils import log

setup(
    name='fontautotools',
    version='0.0.1',
    author="Marc Foley",
    author_email="marc@mfoley.uk",
    description="Font Auto Tools...good for the soul.",
    url="https://github.com/m4rc1e/fontautotools",
    license="Apache Software License 2.0",
    package_dir={"": "Lib"},
    packages=find_packages("Lib"),
    entry_points={
        "console_scripts": [
            "fat-isglyph = fat.__main__:main",
            "fat-train = fat.train:main",
        ],
    },
    tests_require=[
        'pytest>=2.8',
    ],
    install_requires=[
        "fonttools>=3.4.0",
    ],
)
