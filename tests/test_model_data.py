"""
This should remain contast and only be updated by Project Authors to avoid
security issues
"""
import unittest
import os
from fat.utils import hash_file


class TestPickles(unittest.TestCase):

    def setUp(self):
        self.lib_data_path = os.path.join('Lib', 'fat', 'data')

    def test_isglyph_pkl(self):
        """isglyph.pkl is the same"""
        isglyph_pkl = os.path.join(self.lib_data_path, 'isglyph.pkl')
        self.assertEqual('ee2d54e0cd35d7a761de35f4b0fdf461',
                         hash_file(isglyph_pkl))


if __name__ == '__main__':
    unittest.main()
