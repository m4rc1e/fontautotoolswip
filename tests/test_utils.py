from fat.utils import (
    remove_file_extension
)
import unittest 

class TestParser(unittest.TestCase):

    def test_remove_file_extension(self):
        """Remove file extensions"""
        font_name_ttf = 'Roboto-Regular.ttf'
        font_name_otf = 'Roboto-Regular.otf'

        self.assertEqual(remove_file_extension(font_name_ttf), 'Roboto-Regular')
        self.assertEqual(remove_file_extension(font_name_otf), 'Roboto-Regular')


if __name__ == '__main__':
    unittest.main()
