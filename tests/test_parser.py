import os
import unittest
from ntpath import basename
from fontTools.ttLib import TTFont
import numpy as np

from fat.parser import (
    get_glyph_metrics,
    get_glyph_30x30img,
    img_left
)

class TestGlyphData(unittest.TestCase):

    def setUp(self):
        self.font_path = os.path.join('tests', 'data', 'Montserrat-Regular.ttf')
        self.font = TTFont(self.font_path)
        self.test_img = np.array([[1, 0, 0, 2],
                                  [1, 0, 0, 2],
                                  [1, 0, 0, 2]])
    
    def test_get_glyph_metrics(self):
        self.assertEqual((783, 100, 37), get_glyph_metrics('D', self.font))
        # assert it can handle negative lsb and rsb
        self.assertEqual((514, -4, -4), get_glyph_metrics('y', self.font))

    def get_glyph_30x30img(self):
        pass

    def img_left(self):
        self.assertEqual(
            np.array([[1, 0],
                      [1, 0],
                      [1, 0]]),
            get_glyph_img_left(self.test_img)
            )

    def img_right(self):
        self.assertEqual(
            np.array([[0, 2],
                      [0, 2],
                      [0, 2]]),
            get_glyph_img_left(self.test_img)
            )

    def get_glyph_img_right(self):
        pass

if __name__ == '__main__':
    unittest.main()
