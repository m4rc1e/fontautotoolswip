"""
Determine if a font's glyphs are the correct shape
"""
import os
import argparse
import json
from sklearn.linear_model import SGDClassifier
from sklearn.externals import joblib
import numpy as np

from parser import get_font_data
from utils import font_name_from_path


def main():
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('font')
    arg_parser.add_argument('--verbose', '-v', help="Verbose view")

    args = arg_parser.parse_args()

    print('parsing: %s' % args.font)
    font_glyph_data = get_font_data(args.font)

    data_dir = os.path.join(os.path.dirname(__file__), 'data')
    isglyph_pkl = os.path.join(data_dir, 'isglyph.pkl')
    
    print('Loading model')
    classifier = joblib.load(isglyph_pkl) 

    print('Predicting')
    glyphs = font_glyph_data['font']['glyphs']
    for glyph in glyphs:
        glyph_img = font_glyph_data['font']['glyphs'][glyph]['img30x30']
        img_flatten = [list(map(int, np.array(glyph_img).flatten()))]
        print(glyph, str(classifier.predict(img_flatten)),)
        


if __name__ == '__main__':
    main()
