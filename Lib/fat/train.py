"""
Build the training models
"""
import os
import argparse
import json

from parser import get_font_data
from collater import gen_isglyph_dataset
from utils import font_name_from_path, get_files


def main():
    arg_parser = argparse.ArgumentParser(description=__doc__)
    group = arg_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--fonts', '-f', nargs='+', help='Fonts')
    group.add_argument('--directory', '-d')
    arg_parser.add_argument('--recursive', '-r', action='store_true',
                            help="Get fonts recursively")
    args = arg_parser.parse_args()

    data_dir = os.path.join(os.path.dirname(__file__), 'data')
    data_fonts_dir = os.path.join(data_dir, 'fonts')

    if args.fonts:
        fonts = args.fonts
    elif args.directory:
        fonts = [os.path.join(args.directory, f) for f in os.listdir(args.directory)
                 if f.endswith('.ttf')]
    if args.directory and args.recursive:
        fonts = get_files(args.directory)

    for font in fonts:
        print('parsing: %s' % font)
        font_glyph_data = get_font_data(font)
        out_file = os.path.join(data_fonts_dir, '%s.json' % font_name_from_path(font))
        json.dump(font_glyph_data, open(out_file, 'w'), indent=0)

    print('Collating isglyph dataset')
    isglyph_pkl = os.path.join(data_dir, 'isglyph.pkl')
    gen_isglyph_dataset(data_fonts_dir, isglyph_pkl)


if __name__ == '__main__':
    main()
