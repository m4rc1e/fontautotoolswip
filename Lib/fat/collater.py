"""
Convert font data files into meangiful datasets for ML
"""
import json
import csv
import os
import numpy as np 
from sklearn.linear_model import SGDClassifier
from sklearn.externals import joblib

from utils import get_files


def gen_isglyph_dataset(data_path, out_file):
    """Assemble data for isglyph tool"""
    try:
        os.remove(out_file)
    except OSError:
        pass

    data = []
    fonts_data_paths = get_files(data_path, '.json')
    for path in fonts_data_paths:
        d = json.load(open(path))
        for glyph in d['font']['glyphs']:
            glyph_img = d['font']['glyphs'][glyph]['img30x30']
            img_flatten = list(map(int, np.array(glyph_img).flatten()))
            data.append(([glyph] + img_flatten))

    classifier = SGDClassifier(loss="hinge", penalty="l2")

    print('Training model')
    classifier.fit([i[1:] for i in data], [i[0] for i in data])

    print('Saving: %s' % out_file)
    joblib.dump(classifier, out_file)
