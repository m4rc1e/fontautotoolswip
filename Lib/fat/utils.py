"""
Shareable utility objects
"""
import re
from ntpath import basename
import os
import hashlib


def remove_file_extension(font_path):
    """Roboto-Regular.ttf --> Roboto-Regular"""
    return re.sub(r'\.ttf|.otf', r'', font_path)


def font_name_from_path(font_path):
    """path/to/font/Roboto-Regular.ttf --> Roboto-Regular"""
    return basename(remove_file_extension(font_path))


def get_files(root_path, filetype='.ttf'):
    fonts = []
    for path, r, files in os.walk(root_path):
        for f in files:
            if f.endswith(filetype):
                fonts.append(os.path.join(path, f))
    return fonts


def hash_file(f):
    """Return the checksum of a file"""
    checksum = hashlib.md5(open(f, 'rb').read()).hexdigest()
    return checksum
