"""
Extract data from fonts for ML Model
"""
from __future__ import print_function
from fontTools.ttLib import TTFont
from fontTools.pens.areaPen import AreaPen
import subprocess
from ntpath import basename
import re
from PIL import Image
import PIL.ImageOps
import os
import math
import numpy as np
from datetime import datetime
import unicodedata
try:
    from pip import get_installed_distributions
    installed_packages = get_installed_distributions()
except ImportError:
    installed_packages = ['Warning: Non pip install!']
from utils import remove_file_extension, font_name_from_path


def get_font_data(font_path):
    """Process a font's data"""
    font = TTFont(font_path)
    font_name, font_style = font_name_from_path(font_path).split('-')
    italic = True if 'italic' in font_style.lower() else False
    font_data = {'font': {'glyphs': {},
                          'info': {'name': font_name,
                                   'style': font_style,
                                   'italic': italic}},
                          'meta': {'dependencies': get_dependencies(),
                                   'date': str(datetime.now())}
                        }

    encoded_glyphs = font['cmap'].getcmap(3,1).cmap
    glyph_unis, glyph_names = zip(*encoded_glyphs.items())
    for glyph_name, glyph_uni in zip(glyph_names, glyph_unis):
        key = glyph_key(glyph_uni)
        font_data['font']['glyphs'][key] = \
            glyph_data(font_path, font, glyph_name, glyph_uni)
    return font_data


def get_dependencies():
    dependencies = []
    for dependency in installed_packages:
        name = re.sub(r' \(.*\)', r'', str(dependency))
        dependencies.append(name)
    return dependencies


def glyph_key(ordinal):
    """Convert ordinal to uniXXXX based name"""
    return 'uni' + hex(ordinal).replace('0x', '').upper().zfill(4)


def glyph_data(font_path, font, glyph_name, glyph_uni):
    """Get data for an individual character"""
    data = {}
    adv_width, lsb, rsb = get_glyph_metrics(glyph_name, font)
    uni_character = unichr(int(glyph_uni))
    data['lsb'] = lsb
    data['rsb'] = rsb
    data['name'] = glyph_name
    data['adv_width'] = adv_width
    data['img30x30'] = get_glyph_30x30img(font_path, uni_character, glyph_name)
    data['area'] = get_glyph_area(font, glyph_name)
    data['contours'] = get_glyph_contour_count(font, glyph_name)
    data['script'] = get_glyph_script(uni_character)
    return data


def get_glyph_script(uni_character):
    """Return the writing system for the glyph"""
    try:
        script = unicodedata.name(uni_character).split()[0]
    except:
        return "LATIN"
    return script


def get_glyph_metrics(glyph_name, font):
    """Return the adv width, lsb, rsb of a glyph"""
    adv_width = font['hmtx'][glyph_name][0]
    try:
        lsb = font['glyf'][glyph_name].xMin
    except AttributeError:
        lsb = 0
    try:    
        rsb = adv_width - font['glyf'][glyph_name].xMax
    except AttributeError:
        rsb = 0
    return adv_width, lsb, rsb


def get_glyph_30x30img(font_path, char, ord=None):
    """Use harfbuzz and pillow to produce a 30x30 img of a character

    steps:
        - Take hb screenshot of letter
        - Crop screenshot so there is no surrounding whitespace
        - Scale screenshot height first to 20px whilst keeping aspect ratio
        - Pad screenshot with horizontal whitespace so final img is 30x30px
        - Convert it to np array"""
    cwd = os.path.dirname(__file__)
    data_dir = os.path.join(cwd, 'data')
    temp_img = os.path.join(data_dir, 'temp.png')
    try:
        subprocess.call(['hb-view', '--output-file='+temp_img, font_path, '  %s  ' % char])
    except TypeError: # Empty glyph
        return np.full((30,30), 255).tolist()

    img = Image.open(temp_img)
    # Crop it
    img_inverted = PIL.ImageOps.invert(img)
    img_bbox = img_inverted.getbbox()
    # TODO don't clip long horizontal glyphs
    img_cropped = img.crop(box=img_bbox)
    # Scale it
    scale = 20
    hpercent = scale / float(img_cropped.size[1])
    wsize = int(float(img_cropped.size[0]) * float(hpercent))
    if wsize == 0:
        wsize = 1
    img_scaled = img_cropped.resize((wsize, scale), Image.ANTIALIAS)
    # Pad it
    img_framed = PIL.ImageOps.expand(
        img_scaled,
        (int(math.ceil((30 - img_scaled.size[0]) / 2.0)), 5),
        fill=255
    )
    img_fit = PIL.ImageOps.fit(img_framed, (30, 30))
    import random

    img_fit.save(os.path.join(data_dir, 'img_tests', str(ord + '.png')))
    return np.array(img_fit).tolist()


def img_left(img_array):
    """Return left side of image np array"""
    col_d, row_d = img_array.shape
    half = row_d / 2
    return img_array[:, :half]


def img_right(img_array):
    """Return right side of image np array"""
    col_d, row_d = img_array.shape
    half = row_d / 2
    return img_array[:, half:]


def get_glyph_area(font, char):
    """Calculate the surface area of a glyph's ink"""
    glyph_set = font.getGlyphSet()
    area_pen = AreaPen(glyph_set)

    glyph_set[char].draw(area_pen)

    area = area_pen.value
    area_pen.value = 0
    return area


def get_glyph_contour_count(font, char):
    """Contour count for specified glyph.
    This implementation will also return contour count for
    composite glyphs.
    """
    contour_count = 0
    items = [font['glyf'][char]]

    while items:
        g = items.pop(0)
        if g.isComposite():
            for comp in g.components:
                items.append(font['glyf'][comp.glyphName])
        if g.numberOfContours != -1:
            contour_count += g.numberOfContours
    return contour_count
